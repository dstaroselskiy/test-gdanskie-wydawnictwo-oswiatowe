<?php

namespace Recruitment\Entity;

use PhpCollection\Sequence;
use Recruitment\Cart\Item;

/**
 * Description of Order
 *
 * @author dstaroselskiy
 */
class Order
{
    /** @var int */
    protected $id;
    /** @var Sequence */
    protected $items;
    /** @var int */
    protected $total_price;

    /**
     * Cart constructor.
     */
    public function __construct(int $id, Sequence $items)
    {
        $this->setId($id)
            ->setItems($items)
            ->setTotalPrice($this->calcTotalPrice());
    }

    /** Calc and return total order price
     * @return int
     */
    protected function calcTotalPrice(): int
    {
        /** @var array $prices */
        $prices = $this->items->map(function (Item $item) {
            return $item->getTotalPrice();
        })->all();
        return array_sum($prices);
    }

    /** Get orders products items
     * @return Sequence
     */
    public function getItems(): Sequence
    {
        return $this->items;
    }

    /** Set orders products items
     * @param Sequence $items
     * @return Order
     */
    public function setItems(Sequence $items): self
    {
        $this->items = $items;
        return $this;
    }

    /** Return formatted data for order view
     * @return array
     */
    public function getDataForView(): array
    {
        return [
            'id' => $this->getId(),
            'items' => $this->items->map(function (Item $item) {
                return [
                    'id' => $item->getProduct()->getId(),
                    'quantity' => $item->getQuantity(),
                    'total_price' => $item->getTotalPrice()
                ];
            })->all(),
            'total_price' => $this->getTotalPrice()
        ];
    }

    /** Get order id
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /** Set order id
     * @param int $id
     * @return Order
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /** Get orders total price
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->total_price;
    }

    /** Set orders total price
     * @param int $price
     * @return Order
     */
    public function setTotalPrice(int $price): self
    {
        $this->total_price = $price;
        return $this;
    }
}
