<?php

namespace Recruitment\Entity;

use InvalidArgumentException;
use Recruitment\Entity\Exception\InvalidUnitPriceException;

/**
 * Product
 *
 * @author dstaroselskiy
 */
class Product
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $unitPrice = 0;
    /** @var int */
    protected $minimumQuantity = 1;

    /** Get product id
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /** Set product id
     * @param int $id
     * @return Product
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /** Get price by product
     *
     * @return int
     */
    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }

    /** Set price by product
     * @param int $unitPrice
     * @return Product
     * @throws InvalidUnitPriceException
     */
    public function setUnitPrice(int $unitPrice): self
    {
        if ($unitPrice < 1) {
            throw new InvalidUnitPriceException();
        }
        $this->unitPrice = $unitPrice;
        return $this;
    }

    /** Get minimal quantity for products selling
     *
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }

    /** Set minimal quantity for products selling
     * @param int $minimumQuantity
     * @return Product
     * @throws InvalidArgumentException
     */
    public function setMinimumQuantity(int $minimumQuantity): self
    {
        if ($minimumQuantity < 1) {
            throw new InvalidArgumentException('The minimum quantity can not be less then 1');
        }
        $this->minimumQuantity = $minimumQuantity;
        return $this;
    }
}
