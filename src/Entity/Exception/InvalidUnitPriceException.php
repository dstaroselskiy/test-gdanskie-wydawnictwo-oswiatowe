<?php

namespace Recruitment\Entity\Exception;

use Exception;
use Throwable;

/**
 * The Exception of Invalid Unit Price
 *
 * @author dstaroselskiy
 */
class InvalidUnitPriceException extends Exception
{

    /**
     * InvalidUnitPriceException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message??'The unit price can not be less then 1.', $code, $previous);
    }
}
