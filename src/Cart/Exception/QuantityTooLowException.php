<?php

namespace Recruitment\Cart\Exception;

use Throwable;

/**
 * Exception of Quantity Too Low
 *
 * @author dstaroselskiy
 */
class QuantityTooLowException extends \Exception
{

    /**
     * QuantityTooLowException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?? 'Quantity to low.', $code, $previous);
    }
}
