<?php

namespace Recruitment\Cart;

use PhpCollection\Sequence;
use Recruitment\Entity\Order;
use Recruitment\Entity\Product;

/**
 * The Cart by orders products
 *
 * @author dstaroselskiy
 */
class Cart
{
    /** @var Sequence */
    protected $items;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $this->items = new Sequence();
    }

    /** Add product to the cart
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws Exception\QuantityTooLowException
     */
    public function addProduct(Product $product, int $quantity = 1): self
    {
        if ($this->items->isEmpty()) {
            $index = -1;
        } else {
            $index = $this->items->indexWhere(function (Item $item) use ($product) {
                return $item->getProduct() == $product;
            });
        }
        if ($index < 0) {
            $this->items->add(new Item($product, $quantity));
        } else {
            /** @var Item $item */
            $item = $this->items->get($index);
            $item->setQuantity($item->getQuantity() + $quantity);
            $this->items->update($index, $item);
        }
        return $this;
    }

    /** Get list by cart items
     * @return array
     */
    public function getItems(): array
    {
        return $this->items->all();
    }

    /** Get total cart price
     * @return int
     */
    public function getTotalPrice(): int
    {
        if ($this->items->isEmpty()) {
            return 0;
        }
        /** @var array $prices */
        $prices = $this->items->map(function (Item $item) {
            return $item->getTotalPrice();
        })->all();
        return array_sum($prices);
    }

    /** Get order item by index
     * @param int $index
     * @return Item
     */
    public function getItem(int $index): Item
    {
        return $this->items->get($index);
    }

    /** Remove item by product from cart
     * @param Product $product
     * @return bool
     * @throws \Exception
     */
    public function removeProduct(Product $product): bool
    {
        if ($this->items->isEmpty()) {
            throw new \Exception("Cart is empty");
        }
        $index = $this->items->indexWhere(function (Item $item) use ($product) {
            return $item->getProduct() == $product;
        });
        if ($index < 0) {
            return false;
        }
        $this->items->remove($index);
        return true;
    }

    /** Change products item quantity, or add new products item to the cart
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws \Exception
     */
    public function setQuantity(Product $product, int $quantity): self
    {
        if ($this->items->isEmpty()) {
            $index = -1;
        } else {
            $index = $this->items->indexWhere(function (Item $item) use ($product) {
                return $item->getProduct() == $product;
            });
        }
        /** @var Item $item */
        $item = new Item($product, $quantity);
        if ($index < 0) {
            $this->items->add($item);
        } else {
            $this->items->update($index, $item);
        }
        return $this;
    }

    /** Build Order from Cart
     *
     * @param int $id
     * @return Order
     */
    public function checkout(int $id): Order
    {
        $order = new Order($id, $this->items);
        $this->items = new Sequence();
        return $order;
    }
}
