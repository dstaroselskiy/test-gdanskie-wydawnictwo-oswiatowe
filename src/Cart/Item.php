<?php

namespace Recruitment\Cart;

use Recruitment\Entity\Product;
use Recruitment\Cart\Exception\QuantityTooLowException;

/**
 * The item by cart
 *
 * @author dstaroselskiy
 */
class Item
{
    /** @var Product */
    protected $product;
    /** @var int */
    protected $quantity = 1;

    /**
     * Item constructor.
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(Product $product, int $quantity)
    {
        try {
            $this->setProduct($product);
            $this->setQuantity($quantity);
        } catch (\Exception $exception) {
            throw new \InvalidArgumentException();
        }
    }

    /** Calc and return the total price by products carts item
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->getProduct()->getUnitPrice() * $this->getQuantity();
    }

    /** Get product information from carts item
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /** Set product information to carts item
     * @param Product $product
     * @return Item
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /** Get quantity by products carts item
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /** Set quantity to the products carts item
     * @param int $quantity
     * @return Item
     * @throws QuantityTooLowException
     */
    public function setQuantity(int $quantity): self
    {
        if ($this->getProduct()->getMinimumQuantity() > $quantity) {
            throw new QuantityTooLowException();
        }
        $this->quantity = $quantity;
        return $this;
    }
}
